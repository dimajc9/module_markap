let offset = 0;
const sliderLine = document.querySelector('.slider__line');

document.querySelector('.slider__prev').addEventListener('click', function () {
    offset = offset - sliderLine.clientWidth;
    if (offset < 0) {
        offset = sliderLine.clientWidth * (sliderLine.childElementCount - 1);
    }
    sliderLine.style.transform = `translate(${-offset}px)`;
});

document.querySelector('.slider__next').addEventListener('click', function () {
    offset = offset + sliderLine.clientWidth;
    if (offset > sliderLine.clientWidth * (sliderLine.childElementCount - 1)) {
        offset = 0;
    }
    sliderLine.style.transform = `translate(${-offset}px)`;
});